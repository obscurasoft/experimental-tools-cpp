#include <iostream>
#include <math.h>
#include <fstream>
///Because compiller TDM-GCC does not read  endl as the end of the line, 
//we need to define "endl" as "\n"
#define endl "\n"
//Only habit to use namespace std
using namespace std;

/* Expanded calculator */
//Declaring public variable. We will work something unusually.
//Creating logs file
ofstream logging("expcalc.log");
//Declaring some variables and version variable.
void vlog(string inputlog);
string szTest, szTime, _VERSION = "ExpTools v1.0";
float fIn, fIn2, fRes;
bool sCont;
short sCounter, nCase, shCounter = 0; 
void enter();
void enterNum();

//In my incident main arguments give TDM-GCC. Anyway, it is cool compiller!
int main(int argc, char** argv) {
	//Initializing variable for cycle
    sCont = true;
    //Output the programm version
    cout << _VERSION << "\n\n";
    //...and write it to log.
    vlog("==============================================================");
    vlog(_VERSION);
    vlog("==============================================================");
    //Creating input file (must be in system after last session, else create one)
    ifstream in("data.dat");
    vlog("Reading data.dat");
    //Saving data from file, printing and entering main loop.
    in >> fRes;
    in.close();
    cout << "\nLast result: " << fRes << endl;
    //Each new tick - new working step.
    while (sCont != false) { 
      //Print operators
      cout << "Select operation:";
      cout << "\n1 - Add, 2 - Substract,";
      cout << "\n3 - Divide, 4 - Multiplicate,";
      cout << "\n5 - sqrt, 6 - sin,";
      cout << "\n7 - cos, 8 - tg,";
      cout << "\n9 - asin, 10 - acos,";
      cout << "\n11 - atg, 12 - Power,";
      cout << "\n13 - modulo.";
      cout << "\n0 - Exit\n" << endl;
      //Waiting for input.
      vlog("Waiting for input");
      while(!(cin >> nCase)) { 
        cin.clear();
		while(cin.get()!='\n') continue;
		cout<<"\aInput error. Try again: ";
		vlog("Input error. Returning back.");
      }
      logging << "[ExpCalc]" << "> " << "Selected option: " << nCase << endl;
      //Let's be a good boys. No more if - else if - else.
      switch (nCase) {
      		 //Big big big big big case... Simply look at each element to understand
      		 //It's better than trying to comment it.
             case 1:
                enter(); 
		fRes = fIn + fIn2; 
		system("cls"); 
		break;
             case 2:
                enter(); 
		fRes = fIn - fIn2; 
		system("cls"); 
		break; 
             case 3:
                enter(); 
		fRes = fIn / fIn2; 
		system("cls"); 
		break;
             case 4:
                enter(); 
		fRes = fIn * fIn2; 
		system("cls"); 
		break;
             case 5:
                enterNum(); 
		fRes = sqrt(fIn); 
		system("cls"); 
		break;
             case 6:
                enterNum(); 
		fRes = sin(fIn); 
		system("cls"); 
		break;
             case 7:
                enterNum(); 
		fRes = cos(fIn); 
		system("cls"); 
		break;
             case 8:
                enterNum(); 
		fRes = tan(fIn); 
		system("cls"); 
		break;
             case 9:
                enterNum(); 
		fRes = asin(fIn); 
		system("cls"); 
		break;
             case 10:
                enterNum(); 
		fRes = acos(fIn); 
		system("cls"); 
		break;
             case 11:
                enterNum(); 
		fRes = atan(fIn); 
		system("cls"); 
		break;
             case 12:
                enter(); 
		fRes = pow(fIn, fIn2); 
		system("cls"); 
		break;
             case 13:
                enter();
		fRes = (int)fIn % (int)fIn2;
		system("cls");
		break;
             case 0:
                vlog("Exiting");
                sCont = false;
		system("cls");
		cout << "Bye-bye!" << endl;
		break;
             default:
             	//In case of unexepted input - return to menu.
                 vlog("Invalid input. Returning to menu");
                system("cls");
                cout << "Invalid choice\n\n";
      }
      //Printing result (if not exit).
      if(nCase != 0) cout << "Result: " << fRes << endl << endl; 
      vlog("Done.");
    }
    //In case of exit, variable that supports loop sets to False.
    //Creating file for last result
    ofstream out("data.dat");
    vlog("Writing last result to data.dat");
    //Writing result to file and closing program
    out << fRes;
    out.close();
    vlog("Goodbye!");
    vlog("__END__");
    logging.close();
    return 0;
}

//Input. 2 numbers.
void enter() {
     vlog("Vaiting for two numbers.");
     cout << "\nEnter first number: ";
     //Магия. Не трогать.
     while(!(cin >> fIn)) { 
        cin.clear();
	while(cin.get()!='\n') continue;
	cout<<"\aInput error. Try again: ";
	vlog("Invalid input. Returning to first number input.");
     }
     cout << "\nEnter next number: ";
     while(!(cin >> fIn2)) { 
        cin.clear();
	while(cin.get()!='\n') continue;
	cout<<"\aInput error. Try again: "; 
	vlog("Invalid input. Returning to second number input.");
     }
}

//Logging.
void vlog(string inputlog) {
	logging << "[ExpCalc]> " << inputlog << endl;
}

//Input. 1 number.
void enterNum() {
     vlog("Waiting for number");
     cout << "\nEnter number: ";
     while(!(cin >> fIn)) { 
        cin.clear();
	while(cin.get()!='\n') continue;
	cout<<"\aInput error. Try again: ";
	vlog("Invalid input. Returning to number input.");
     }
}   
